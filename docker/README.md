This directory contains additional files for the Docker image build.

See the [Dockerfile](../Dockerfile) for more details.
