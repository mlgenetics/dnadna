.. DNADNA documentation master file, created by
   sphinx-quickstart on Fri Mar 20 16:38:13 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

********************
DNADNA Documentation
********************

.. Include the README as the top-level introduction to the documentation,
.. starting at line 3 to skip the README's top-level header

.. mdinclude:: ../README.md
   :start-line: 3
   :end-line: 15


Full Documentation
==================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   overview
   tutorials
   configuration
   datasets
   data_preprocessing
   training
   prediction
   simulator
   schemas
   extending
   development
   api

.. TODO: Add documentation for the examples module as well.


Changelog
=========

.. mdinclude:: ../CHANGELOG.md
   :start-line: 3


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
