.. Contains the rest of the contents from README.md, but in a separate
   document from index, so that its sub-sections appear in the ToC
   (related to this issue, for which a reorganization like this seems to be
   the recommended workaround
   https://github.com/readthedocs/sphinx_rtd_theme/issues/445)

Introduction
############

.. mdinclude:: ../README.md
   :start-line: 16
